import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from '../video-types';
import { VideoLoaderService } from '../../api/video-loader.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  dashboardVideos: Observable<Video[]>;
  selectedVideo: Video | undefined;

  constructor(vls: VideoLoaderService) {
    this.dashboardVideos = vls.loadVideos();
  }

  ngOnInit(): void {
  }

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
  }

}
